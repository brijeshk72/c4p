# Generated by Django 2.2 on 2019-11-25 04:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0011_forcelearning'),
    ]

    operations = [
        migrations.AddField(
            model_name='forcelearning',
            name='force_time_model',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='forcelearning',
            name='force_time_quiz',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
