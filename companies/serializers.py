from .models import *
from django.contrib.auth.models import Group
from .mixins import RepresentationMixin
from users.models import User


class BranchSerializer(RepresentationMixin):
    class Meta:
        model = Branch
        fields = ['id', 'name', 'address', 'country', 'state', 'city',
                  'zip_code', 'company', 'active', 'first_name', 'email', 'mobile']


class UserWorkProfileSerializer(RepresentationMixin):
    branches = BranchSerializer(many=True, read_only=True)

    class Meta:
        model = UserWorkProfile
        fields = ['company', 'user', 'user_type', 'branches', 'active']


class InternalCommitteeSerializer(RepresentationMixin):
    class Meta:
        model = InternalCommitee
        fields = ['id', 'userprofile', 'name', 'gender',
                  'mobile', 'email', 'isheadOfficer', 'company']


class CompanySerializer(RepresentationMixin):
    branches = BranchSerializer(many=True, read_only=True)
    company_work_profile = UserWorkProfileSerializer(many=True, read_only=True)
    company_committee = InternalCommitteeSerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = ['id', 'name', 'address', 'country', 'state', 'city', 'zip_code', 'branches', 'company_work_profile',
                  'company_committee', 'active']


class ForceLearningSerializer(RepresentationMixin):
    class Meta:
        model = ForceLearning
        fields = "__all__"
