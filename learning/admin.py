from django.contrib import admin
from easy_select2 import select2_modelform
from .models import *

# Register your models here.

SelectForm = select2_modelform(Chapter, attrs={'width': '500px'})
#


@admin.register(Chapter)
class ChapterAdmin(admin.ModelAdmin):
    model = Chapter
    list_display = ['id', 'title', 'language']
    list_filter = ['title', 'language']
    search_fields = ('title', )
#
    form = SelectForm


SelectForm = select2_modelform(Section, attrs={'width': '500px'})
#
@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    model = Chapter
    list_display = ['id', 'title', 'chapter']
    list_filter = ['chapter', 'title']
    search_fields = ('title', )
#
    form = SelectForm


SelectForm = select2_modelform(Quiz, attrs={'width': '500px'})
#
@admin.register(Quiz)
class QuizAdmin(admin.ModelAdmin):
    model = Quiz
    list_display = ['id', 'title', 'chapter', 'section', 'forced_learning']
    list_filter = ['chapter', 'section']
    search_fields = ('chapter__title', )
#
    form = SelectForm


SelectForm = select2_modelform(Question, attrs={'width': '500px'})

# @admin.register(Question)
# class QuestionAdmin(admin.ModelAdmin):
#     model = Question
#     list_display = ['id','title','quiz']
#     list_filter = ['quiz__title','quiz__section__title' ]
#     search_fields = ('title','quiz__title' )
# #
#     form = SelectForm

# SelectForm = select2_modelform(Option, attrs={'width': '500px'})
#
# @admin.register(Option)
# class OptionAdmin(admin.ModelAdmin):
#     model = Option
#     list_display = ['id','title','question','is_correct']
#     list_filter = ['question__quiz__title','question__quiz__section__title' ]
#     search_fields = ('title','question__quiz__title' )
# #
#     form = SelectForm


class OptionInline(admin.TabularInline):
    model = Option


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'timer', 'quiz', 'score')
    inlines = [OptionInline]

    class Meta:
        model = Question


admin.site.register(Question, QuestionAdmin)


# admin.site.register(Option)

class AnswerAdmin(admin.ModelAdmin):
    list_display = ('question', 'option', 'quiz', 'user')


admin.site.register(Answer, AnswerAdmin)


# admin.site.register(Answer)
# admin.site.register(Quiz_Result)


class Quiz_ResultAdmin(admin.ModelAdmin):
    list_display = ('user', 'quiz', 'obtain_marks', 'total_marks')


admin.site.register(Quiz_Result, Quiz_ResultAdmin)


admin.site.register(Stories)
