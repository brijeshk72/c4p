# Generated by Django 2.2 on 2019-10-10 07:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('learning', '0009_chapter_company'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='chapter',
            name='company',
        ),
    ]
