from django.urls import path
from learning.views import *
from . import views


app_name = 'learning'

urlpatterns = [
    # path('api/learning/api_chapter_view/', views.api_chapter_view, name='api_chapter_view'),
    # path('api/learning/api_section_view/', views.api_section_view, name='api_section_view'),
    # path('api/learning/api_quiz_view/', views.api_quiz_view, name='api_quiz_view'),
    # path('api/learning/api_question_view/', views.api_question_view, name='api_question_view'),
    # path('api/learning/api_option_view/', views.api_option_view, name='api_option_view'),
    # path('api/learning/api_answer_view/', views.api_answer_view, name='api_answer_view'),

    path('api/learning/chapter/', views.ChapterView.as_view(), name='chapterview'),
    path('api/language/', views.LanguageView.as_view(), name='language'),
    path('api/learning/chapter/<int:pk>/',
         views.Chapter_View.as_view(), name='chapter_view'),

    path('api/learning/section/', views.SectionView.as_view(), name='sectionview'),
    path('api/learning/chapter/section/<int:pk>/',
         views.Section_View.as_view(), name='section_view'),
    path('api/learning/chapter/sections/<int:pk>/',
         views.Section_Views.as_view(), name='section_views'),

    path('api/learning/quiz/', views.QuizView.as_view(), name='quizview'),
    path('api/learning/quiz/<int:pk>/',
         views.Quiz_View.as_view(), name='quiz_view'),

    path('api/learning/question/',
         views.QuestionView.as_view(), name='questionview'),
    path('api/learning/question/quiz/<int:pk>/',
         views.Question_View.as_view(), name='question_view'),

    path('api/learning/option/', views.OptionView.as_view(), name='optionview'),
    path('api/learning/option/<int:pk>/',
         views.Option_View.as_view(), name='option_view'),

    path('api/learning/answer/', views.AnswerView.as_view(), name='answerview'),
    path('api/learning/answer/<int:pk>/',
         views.Answer_View.as_view(), name='answer_view'),

    path('api/learning/result/', views.Quiz_Result_View.as_view(),
         name='Quiz_Result_View'),
    path('api/learning/result/<int:pk>/',
         views.Quiz_Result_Views.as_view(), name='Quiz_Result_Views'),


    path('api/learning/stories/', views.StoriesView.as_view(), name='storiesview'),
    path('api/learning/stories/<int:pk>/',
         views.Stories_View.as_view(), name='stories_view'),

    path('api/learning/forced_learning/',
         views.ForcedLearningView.as_view(), name='forcedearning'),
]


# from django.urls import path
# from . import views
# app_name = 'learning'

# urlpatterns = [
#     path('api/learning/chapter/',views.ChapterGetView.as_view(),name='chapter'),
# ]
