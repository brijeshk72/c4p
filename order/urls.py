from django.urls import path
from . import views
app_name = 'order'

urlpatterns = [
    path('api/order/',views.api_order_view,name='order')
]