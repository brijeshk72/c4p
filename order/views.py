from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import *
from license.models import LicenseModel
from .serializers import *
from django.http import Http404
import json

#chapter Api
@api_view(['GET', 'POST'])
def api_order_view(request):
    # try:
    #     order = Order.objects.get(id=1)
    # except:
    #     return Response('Id Does not exists',status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':  

        try:
            order = Order.objects.get(id=1)
            serializer = OrderItemSerializer(order)
            return Response(serializer.data)
        except:
            return Response('Id Does not exists',status=status.HTTP_404_NOT_FOUND)

        

    if request.method == 'POST':
        data_json = json.loads(request.body.decode("utf-8"))

        company_name = data_json['company_name']
        paid = data_json['paid']
        payumoney_id = data_json['payumoney_id']
        coupon = Coupon.objects.get(id=data_json['coupon'])
        discount = data_json['discount']
        first_name = data_json['first_name']
        last_name = data_json['last_name']
        mobile = data_json['mobile']
        email = data_json['email']
        gender = data_json['gender']
        address = data_json['address']
        country = data_json['country']
        state = data_json['state']
        city = data_json['city']
        zip_code = data_json['zip_code']
        created = data_json['created']
        updated = data_json['updated']
        active = data_json['active']

        Order.objects.create(company_name=company_name, paid=paid, payumoney_id=payumoney_id,
        coupon=coupon, discount=discount, first_name=first_name, last_name=last_name, mobile=mobile,
        email=email, gender=gender, address=address, country=country, state=state, city=city,
        zip_code=zip_code, created=created, updated=updated, active=active)

        order = Order.objects.get(id= data_json['order'])
        module = Module.objects.get(id= data_json['module'])
        license = LicenseModel.objects.get(license= data_json['license'])
        payable_amount = data_json['payable_amount']
        license_quantity = data_json['license_quantity']

        OrderItem.objects.create(order=order, module=module, license=license,payable_amount=payable_amount, license_quantity=license_quantity)

        return Response('1 Order created', status=status.HTTP_201_CREATED)
    else:
        return Response('Order does not Placed some error issue', status=status.HTTP_400_BAD_REQUEST)
