from django.db import models
from core.models import *
from coupons.models import Coupon
from django.core.validators import MinValueValidator, \
                                   MaxValueValidator

from modules.models import Module, License
from license.models import LicenseModel
# Create your models here.


class Order(ProfileMixin, AddressMixin, StatusMixin):
	company_name = models.CharField(max_length=100, null=True, blank=True)
	paid = models.BooleanField(default=False)
	payumoney_id = models.CharField(max_length=150, blank=True)
	coupon = models.ForeignKey(Coupon,
	                           related_name='orders',
	                           null=True,
	                           blank=True,
	                           on_delete=models.SET_NULL)
	discount = models.IntegerField(default=0,
	                               validators=[MinValueValidator(0),
	                                           MaxValueValidator(100)])

	# def __str__(self):
	# 	return self.company_name

class OrderItem(models.Model):
    order = models.OneToOneField(Order,
                              related_name='items',
                              on_delete=models.CASCADE)
    module = models.ForeignKey(Module,
                                related_name='order_module',
                                on_delete=models.CASCADE)
    license= models.ForeignKey(LicenseModel,
                                related_name='order_license',
                                on_delete=models.CASCADE)    
    payable_amount = models.DecimalField(max_digits=10, 
    									decimal_places=2, 
    									default=0.00,
    									null=True,
    									blank=True,
    									help_text='Leave this field empty.')
    license_quantity = models.PositiveIntegerField(default=10)


    def save(self, *args, **kwargs):
    	self.payable_amount = self.get_cost()
    	super().save(*args, **kwargs)

    def get_cost(self):
        return self.module.price + (self.license.price * self.license_quantity)

