from django.shortcuts import render
from companies.mixins import DispatchLoginMixin
from .models import *
from .serializers import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.http import Http404
import json

class ComplaintCreateView(DispatchLoginMixin):

    def post(self,request,*args,**kwargs):

        try:
            comComplaint = CompanyComplaints.objects.get(id=request.POST['comp_id'])
            comComplaint.is_resolved = request.POST['status']
            comComplaint.save()
            return Response('Status updated.')
        except KeyError:
            try:
                complainD=ComplaintDetail.objects.create(
                    accussed_name = request.POST['accussed_name'], 
                    accussed_department = request.POST['accussed_department'], 
                    accussed_position = request.POST['accussed_position'], 
                    repeat_times  = request.POST['repeat_times'],
                    severity = request.POST['severity'], 
                    description = request.POST['description'],
                    attachment = request.FILES['attachment']
                    )
                CompanyComplaints.objects.create(
                    company_name= Company.objects.get(id=self.valid_user.id),
                    branch_name = Branch.objects.get(id=self.valid_user.id),
                    complaints_by = UserWorkProfile.objects.get(id=self.valid_user.id),
                    complaint_details = ComplaintDetail.objects.get(id=complainD.id),
                    anonymous = request.POST.get('is_anonymous')
                    )
                return Response('complains created', status=status.HTTP_201_CREATED)
            except KeyError:
                return Response('Some Data Are Missing, Please Fill Before Submit')

        

    def get(self,request,*args,**kwargs):
        company = self.valid_user.user_profile.company
        try:
            complaints = CompanyComplaints.objects.filter(company_name_id=company.id).order_by('-id')
            if complaints is None:
                return Response('Data Not Found')

            complain_list = []
            for complain in complaints:
                if complain.anonymous:
                    person = 'anonymous'
                else:
                    person = complain.complaints_by.user.username

                if complain.is_resolved:
                    status = 'Resolved'
                else:
                    status = 'Pending'

                complain_list.append({
                    'complain_id':complain.id,
                    'accussed_name':complain.complaint_details.accussed_name,
                    'accussed_department':complain.complaint_details.accussed_department,
                    'accussed_position':complain.complaint_details.accussed_position,
                    'repeat_times':complain.complaint_details.repeat_times,
                    'severity':complain.complaint_details.get_severity_display(),
                    'description':complain.complaint_details.description,
                    'attachment':complain.complaint_details.attachment.url,
                    'company':complain.company_name.name,
                    'branch':complain.branch_name.name,
                    'complaint_by':person,
                    'status': status,
                    'created_date':complain.created.strftime("%d/%m/%Y")
                    
                })
            return Response(complain_list)
        except CompanyComplaint.DoesNotExist:
            return Response('Data Not Found')

class ComplaintCreateViews(DispatchLoginMixin):
    def get(self, request, pk):
        try:
            complain = CompanyComplaints.objects.get(id=pk)
            if complain is None:
                return Response('Data Not Found')

            if complain.anonymous:
                person = 'anonymous'
            else:
                person = complain.complaints_by.user.username

            complain_list ={
                'complain_id':complain.id,
                'accussed_name':complain.complaint_details.accussed_name,
                'accussed_department':complain.complaint_details.accussed_department,
                'accussed_position':complain.complaint_details.accussed_position,
                'repeat_times':complain.complaint_details.repeat_times,
                'severity':complain.complaint_details.get_severity_display(),
                'description':complain.complaint_details.description,
                'attachment':complain.complaint_details.attachment.url,
                'company':complain.company_name.name,
                'branch':complain.branch_name.name,
                'complaint_by':person,
                'status':complain.is_resolved,
            }
            return Response(complain_list)
            
        except CompanyComplaints.DoesNotExist:
            return Response('Data Not Found')


            

#Quey Message views HEre
class QueryMessageViews(DispatchLoginMixin):

    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body)
        try:
            QueryMessage.objects.create(
                                # query=request.POST.get('query'),
                                query =data_json['query'],
                                attached_files = request.FILES.get('attached_files'),
                                # attached_files = data_json['attached_files').url,
                                user = self.valid_user
                                )
            return Response("Query has been submitted")
        except QueryMessage.DoesNotExist:
            return Response('Some file Missing, Please try Again.')

    def get(self, request, *args, **kwargs):
        queries = QueryMessage.objects.all().order_by('-id')
        serializer = QueryMessageSerializer(queries, many=True)
        return Response(serializer.data)
        


class QueryMessage_Views(DispatchLoginMixin):
    def get(self, request, pk):
        try:
            queries = QueryMessage.objects.get(pk=pk)
            serializer = QueryMessageSerializer(queries)
            return Response(serializer.data)
        except QueryMessage.DoesNotExist:
            return Response('Data Not Found')