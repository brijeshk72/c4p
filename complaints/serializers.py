from companies.mixins import RepresentationMixin
from .models import *
from rest_framework import serializers
from companies.serializers import *
# from users.serializers import UserSerializer

class ComplaintDetailSerializer(RepresentationMixin):
    class Meta:
        model = ComplaintDetail
        fields = ['id', 'accussed_name', 'accussed_department', 'accussed_position', 'severity', 'description', 'attachment']

class ComplaintSerializer(RepresentationMixin):
    # complaint_detail = ComplaintDetailSerializer(read_only=True)
    # company = serializers.CharField(source='company.name', read_only=True)
    # branch = serializers.CharField(source='branch.name', read_only=True)

    class Meta:
        model = CompanyComplaint
        fields = ['id','company','branch','accepted_by','complaint_by','complaint_detail','is_resolved','is_anonymous','created']


class CompanyComplaintsSerializer(RepresentationMixin):
    class Meta:
        model = CompanyComplaints
        fields = "__all__"

class ComplainReceiveSerializer(RepresentationMixin):
    class Meta:
        fields = "__all__"


class QueryMessageSerializer(RepresentationMixin):
    user = serializers.CharField(source='user.username', read_only=True)
    class Meta:
        model = QueryMessage
        fields = ['id','query', 'attached_files', 'user', 'updated', 'created', 'active']
