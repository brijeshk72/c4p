from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(CompanyComplaint)
admin.site.register(ComplaintDetail)
admin.site.register(QueryMessage)


class CompanyComplaintsAdmin(admin.ModelAdmin):
    list_display = ['id', 'complaints_by', 'company_name', 'branch_name', 'anonymous']

admin.site.register(CompanyComplaints, CompanyComplaintsAdmin)
