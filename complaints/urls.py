from django.urls import path
from . import views
app_name = 'complaints'

urlpatterns = [
    path('api/complain/',views.ComplaintCreateView.as_view(),name='complain'),
    path('api/complain/<int:pk>/',views.ComplaintCreateViews.as_view(),name='complains'),

    path('api/query/',views.QueryMessageViews.as_view(),name='QueryMessageViews'),
    path('api/query/<int:pk>/',views.QueryMessage_Views.as_view(),name='QueryMessage_Views')
]