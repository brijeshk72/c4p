from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import *
from .serializers import *
from django.http import Http404
import json

#chapter Api
@api_view(['GET', 'POST'])
def api_module_view(request):
 
    if request.method == 'GET':  
        module = Module.objects.get(id=1)
        serializer = ModuleSerializer(module)
        return Response(serializer.data)

    if request.method == 'POST':
        data_json = json.loads(request.body.decode("utf-8"))

        name = data_json['name']
        slug = data_json['slug']
        description = data_json['description']
        price = data_json['price']
        active = data_json['active']
        
        Module.objects.create(name=name, slug=slug, description=description, price=price, active=active)

        return Response('1 Module created', status=status.HTTP_201_CREATED)
    else:
        return Response('Module not created some error issue', status=status.HTTP_400_BAD_REQUEST)


