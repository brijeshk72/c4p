from django.contrib import admin
from .models import Module, License
# Register your models here.

@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
	list_display = ['name', 'price', 'active', 'updated', 'created']
	list_editable = ['active', 'price']
	prepopulated_fields = {'slug': ('name',), }


@admin.register(License)
class ModuleAdmin(admin.ModelAdmin):
	list_display = ['name', 'price', 'active', 'updated', 'created']
	list_editable = ['active', 'price']
	prepopulated_fields = {'slug': ('name',), }
