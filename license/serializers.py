from companies.mixins import RepresentationMixin
from .models import LicenseModel


class LicenseSerializer(RepresentationMixin):

    class Meta:
        model = LicenseModel
        # fields = "__all__"
        fields = ['uuid', 'license', 'added_by', 'user_profile', 'branch']
        extra_kwargs = {'license': {'read_only': True}}
