import json
from companies.mixins import DispatchLoginMixin, DirectorDispatchMixin
from users.models import User
from companies.models import UserWorkProfile, Branch
from rest_framework.response import Response
from .models import LicenseModel, CompanyBuffer
from .serializers import LicenseSerializer
from rest_framework.authtoken.models import Token
from django.core.mail import send_mail
# from rest_framework.views import APIView
# Create your views here.


class EmployeeCreateRetrieveView(DirectorDispatchMixin):

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        company = self.work_profile.company

        try:
            buffer = CompanyBuffer.objects.get(company=company)
            licenses_created = LicenseModel.objects.filter(
                company=company).count()
            if licenses_created >= buffer.total_licenses:
                return Response({
                    "status": False,
                    "message": "No More Licenses Available"
                })
        except CompanyBuffer.DoesNotExist:
            return Response({
                "status": False,
                "message": "License Number Not Defined"
            })
        user = User.objects.filter(email=data.get('email')).first()
        if user:
            return Response({
                "status": False,
                "message": "Email Already Exist"
            })
        branch = Branch.objects.get(pk=int(data.get('branch')))
        user = User.objects.create(
            username=data.get('email'),
            email=data.get('email'),
            mobile=data.get('mobile'),

        )

        token, created = Token.objects.get_or_create(user=user)
        emp_user_profile = UserWorkProfile.objects.create(
            user=user,
            company=company,
            user_type=3,
        )
        emp_user_profile.save()
        emp_user_profile.branches.add(branch)
        license = LicenseModel.objects.create(
            user_profile=emp_user_profile,
            company=company,
            branch=branch,
        )

        serialized_license = LicenseSerializer(license)
        buffer = CompanyBuffer.objects.get(company=company)
        buffer.total_licenses -= 1
        buffer.save()
        send_mail('C4P issued a License', license.license, 'centre4posh@gmail.com', [
                  data.get('email')], fail_silently=False)

        return Response(
            {
                "license": serialized_license.data,
                "token": token.key
            }
        )

    def get(self, request, *args, **kwargs):
        company = self.work_profile.company
        print(company, '==============')

        licenses = LicenseModel.objects.filter(company=company)
        license_list = []
        for lic in licenses:
            license_list.append({
                "UUID": lic.uuid,
                "license": lic.license,
                "user_profile": lic.user_profile.user_type,
                "company": lic.company.name,
                "branch": lic.branch.name,
                # "added_by": lic.added_by,
                'gender': lic.gender,
                "active_license": lic.active_license
            })

        total_license = CompanyBuffer.objects.get(company=company)

        license_accessed = LicenseModel.objects.filter(
            active_license=True, company=company).count()

        return Response({
            "total_license": total_license.total_licenses,
            "license_issued": licenses.count(),
            "license_left": total_license.total_licenses-licenses.count(),
            "license_accessed": license_accessed,
            "license_unaccessed": licenses.count() - license_accessed,
            "license_male": LicenseModel.objects.filter(gender='M', active_license=True, company=company).count(),
            "license_female": LicenseModel.objects.filter(gender='F', active_license=True, company=company).count(),
            "license_list": license_list
        }
        )


class LicenseViews(DispatchLoginMixin):
    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body)
        try:
            license = data_json['license']
            lic = LicenseModel.objects.get(license=license)
            if lic.active_license == False:
                lic.active_license = True
                lic.save()

            return Response(lic.license)
        except LicenseModel.DoesNotExist:
            return Response("Please Enter the valid License Number")
