from django.contrib import admin
from .models import *
# Register your models here.
# admin.site.register(LicenseModel)
# admin.site.register(CompanyBuffer)


class CompanyBufferAdmin(admin.ModelAdmin):
    list_display = ('total_licenses', 'company')


admin.site.register(CompanyBuffer, CompanyBufferAdmin)


class LicenseModelAdmin(admin.ModelAdmin):
    list_display = ('license', 'company',
                    'active_license', 'gender', 'added_by')


admin.site.register(LicenseModel, LicenseModelAdmin)
