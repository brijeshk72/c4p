from django.urls import path
from .views import EmployeeCreateRetrieveView, LicenseViews
app_name = 'license'

urlpatterns = [
    path('api/license/', EmployeeCreateRetrieveView.as_view(),
         name='employee_license'),
    path('api/license/login/', LicenseViews.as_view(), name='licenseviews'),
]
