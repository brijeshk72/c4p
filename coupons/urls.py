from django.urls import path
# from rest_framework.urlpatterns import format_suffix_patterns
from coupons.views import CouponSerializer
from . import views


app_name = 'coupons'


urlpatterns = [
    path('api/coupon/',views.CouponView.as_view(),name='couponview'),
    path('api/coupon/<int:pk>/',views.Coupon_View.as_view(),name='coupon_view')
]

# urlpatterns = format_suffix_patterns(urlpatterns)