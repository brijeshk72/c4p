from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import *
from .serializers import *
from django.http import Http404
import json
from companies.mixins import DispatchLoginMixin


class CouponView(DispatchLoginMixin):
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))
        code = data_json['code']
        valid_from = data_json['valid_from']
        valid_to = data_json['valid_to']
        discount = data_json['discount']
        active = data_json['active']
        
        Coupon.objects.create(code=code, valid_from=valid_from, valid_to=valid_to, discount=discount, active=active)

        return Response('Coupon created', status=status.HTTP_201_CREATED)

    def get(self,request,*args,**kwargs):
        
        coupon = Coupon.objects.all()
        print(coupon)
        serializer = CouponSerializer(coupon, many=True)
        return Response(serializer.data)


class Coupon_View(DispatchLoginMixin):
    """
    Retrieve, update or delete a coupon instance.
    """
    def get_object(self, pk):
        try:
            return Coupon.objects.get(pk=pk)
        except Coupon.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        coupon = self.get_object(pk)
        serializer = CouponSerializer(coupon)
        return Response(serializer.data)

    def put(self, request,pk):
        coupon = self.get_object(pk)
        serializer = CouponSerializer(coupon, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def delete(self, request, pk):
        coupon = self.get_object(pk)
        coupon.delete()
        return Response('object deleted', status=status.HTTP_204_NO_CONTENT)
   