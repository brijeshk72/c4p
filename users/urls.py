from django.urls import path,include
from . import views
app_name = 'users'

urlpatterns = [
    path('api/signup/',views.UserCreate.as_view(), name='signup'),

    path('api/sos/', views.SOSView.as_view(), name='sos'),
    path('api/sos/<int:pk>/', views.SOSViews.as_view(), name='soss')
]
