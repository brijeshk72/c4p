from django.contrib.auth.models import Group
from rest_framework import serializers
from .models import User, SOS
from companies.mixins import RepresentationMixin

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'password','mobile')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
        email=validated_data['email'],
        username=validated_data['username'],
        mobile = validated_data['mobile']
        )
        user.set_password(validated_data['password'])
        user.is_director = True
        user.save()
        g = Group.objects.get(name="Director")
        user.groups.add(g)
        return user


class SOSSerializer(RepresentationMixin):
    class Meta:
        model = SOS
        fields = "__all__"
